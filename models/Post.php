<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property string $category
 * @property string $author
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Post extends \yii\db\ActiveRecord
{public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
           
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           // [['created_at', 'updated_at'], 'safe'],
            //[['created_by', 'updated_by'], 'integer'],
            [['title', 'body', 'category', 'author', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
           'created_at' => 'Created At',
           'updated_at' => 'Updated At',
           'created_by' => 'Created By',
           'updated_by' => 'Updated By',
        ];
    }
    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }
    public function getStatus(){
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'author']);
    }
}
