<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Category;
use app\models\User;
use dosamigos\selectize\SelectizeTextInput;
use dosamigos\selectize\SelectizeDropDownList;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->dropDownList(
        ArrayHelper::map(Category::find()->asArray()->all(),'id','category_name')
    ) ?>
     <?php  if (\Yii::$app->user->can('updateAllPost')) { ?>
    <?= $form->field($model, 'author')->dropDownList(
        ArrayHelper::map(User::find()->asArray()->all(),'id','name')
    ) ?>
     <?php } ?>
    <?php  if (\Yii::$app->user->can('updateAllPost')) { ?>
        <?= $form->field($model, 'status')->dropDownList(
        ArrayHelper::map(Status::find()->asArray()->all(),'id','status_name')
    ) ?>
    <?php } ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
